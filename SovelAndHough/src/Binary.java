import org.w3c.dom.css.RGBColor;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Zayed on 8/3/2016.
 */
public class Binary {

    int maxPosx = 0, maxposY = 0, minX = 99999, minY = 99999;
    LinkedList<Integer> pattern;

    public void rgbToGray() {

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JPG & GIF Images", "jpg", "gif");
        chooser.setFileFilter(filter);
        chooser.showOpenDialog(null);

        int[][] grayMat;

        File imageFile = chooser.getSelectedFile();

        BufferedImage inputImage = null, outputImage = null, houghOutput = null;
/*        String fileName = "D:/Java projects/SovelAndHough/src/android.jpg";
        String grayImageName = "D:/Java projects/SovelAndHough/src/gray.jpg";
        String houghImageName = "D:/Java projects/SovelAndHough/src/android - Copy.jpg";*/
        try {
//			URL url = getClass().getResource(fileName);
            inputImage = ImageIO.read(imageFile);
            outputImage = ImageIO.read(createNewImageInParentDirectory("grayImage.png", inputImage));
            houghOutput = ImageIO.read(createNewImageInParentDirectory("houghMock.png", inputImage));
/*
            outputImage = ImageIO.read(new File(grayImageName));
*/
/*
            houghOutput= ImageIO.read(new File(houghImageName));
*/
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        grayMat=new int[inputImage.getWidth()][inputImage.getHeight()];
        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = getGrayScaleValue(pixel);
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
                grayMat[posx][posy]=grayValue;
            }
        }

        int threeshold = getThreshold(grayMat);

        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = getGrayScaleValue(pixel);


                if (grayValue < threeshold) {
                    houghOutput.setRGB(posx, posy, new Color(255, 255, 255).getRGB());
                } else {
                    houghOutput.setRGB(posx, posy, new Color(0, 0, 0).getRGB());
                }
            }
        }
        createNewImageInParentDirectory("grayScale.png", outputImage);

        File out = createNewImageInParentDirectory("binary.png", houghOutput);

        BufferedImage binaryOutput = null;
        try {
            binaryOutput = ImageIO.read(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedImage binInput = null;

        try {
            binInput = ImageIO.read(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        pattern = new LinkedList<>();

        for (int posy = 0; posy < binInput.getHeight(); posy++) {
            int whitePixelCount = 0;
            for (int posx = 0; posx < binInput.getWidth(); posx++) {
                if (rgbToGrayValue(binInput.getRGB(posx, posy)) == 255) {
                    whitePixelCount++;
                }
            }

            if (whitePixelCount > 1) {
                for (int posx = 0; posx < binInput.getWidth(); posx++) {
                    houghOutput.setRGB(posx, posy, new Color(0, 0, 0).getRGB());
                }

/*
                System.out.println(posy + "");
*/
                pattern.add(posy);

            }

        }

        out = createNewImageInParentDirectory("Matrahin.png", houghOutput);

//              word parsing
        BufferedImage matInput = null, matOutput = null;


        try {
            matInput = ImageIO.read(imageFile);
            matOutput = ImageIO.read(createNewImageInParentDirectory("MatOut", binaryOutput));
        } catch (IOException e) {
            e.printStackTrace();
        }

        int wordCount = 0;
        int size = pattern.size();
        for (int i = 0; i < size; i++) {

            Integer posy = pattern.removeFirst() + 1;

/*
            System.out.println(posy + "");
*/

            for (int posx = 1; posx < matOutput.getWidth(); posx++) {
                if (rgbToGrayValue(matOutput.getRGB(posx, posy)) == 255) {
                    maxPosx = posx;
                    maxposY = posy;
                    minX = posx;
                    minY = posy;
                    getConnectedPixels(posx, posy, 255, matOutput);
                    wordCount++;

                    if (cropImage(matInput, minX, minY, maxPosx, maxposY) != null) {
                        createNewWordImageInImageDirectory("words\\" + wordCount + ".png", cropImage(matInput, minX, minY, maxPosx, maxposY));
                        createNewWordImageInImageDirectory("wordsBin\\" + wordCount + ".png", cropImage(binaryOutput, minX, minY, maxPosx, maxposY));
                    }
                }
            }
        }

        createNewImageInParentDirectory("REddddd.png", matOutput);
/*
        System.out.println("WordCount :" + wordCount);
*/

        BufferedImage matOutput2 = null;

        BufferedImage image = null, imageCopy = null;
        int letterCount = 0;
        for (int lc = 1; lc < wordCount; lc++) {
            try {
                image = ImageIO.read(new File("D:\\Java projects\\SovelAndHough\\src\\wordsBin\\" + lc + ".png"));

                imageCopy = image;
            } catch (Exception e) {
                e.printStackTrace();

                System.out.println(lc);

                continue;
            }

            for (int posy = 0; posy < image.getHeight(); posy++) {
                int whitePixelCount = 0;
                for (int posx = 0; posx < image.getWidth(); posx++) {
                    if (rgbToGrayValue(image.getRGB(posx, posy)) == 255) {
                        whitePixelCount++;
                    }
                }

                if (whitePixelCount > image.getWidth() / 1.5 && whitePixelCount > 10) {
                    for (int posx = 0; posx < image.getWidth(); posx++) {
                        imageCopy.setRGB(posx, posy, new Color(0, 0, 0).getRGB());
                    }

/*
                System.out.println(posy + "");
*/

                }

            }

            File wordBinaryMatra = createNewImageInParentDirectory("\\wordMatra\\" + lc + ".png", imageCopy);
            BufferedImage wordBinaryMatraImage = null;
            try {
                wordBinaryMatraImage = ImageIO.read(wordBinaryMatra);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int posy = wordBinaryMatraImage.getHeight() / 2;
            for (int posx = 1; posx < wordBinaryMatraImage.getWidth(); posx++) {
                if (rgbToGrayValue(wordBinaryMatraImage.getRGB(posx, posy)) == 255) {
                    maxPosx = posx;
                    maxposY = posy;
                    minX = posx;
                    minY = posy;
                    getConnectedPixels(posx, posy, 255, wordBinaryMatraImage);
                    letterCount++;

                    if (cropImage(matInput, minX, minY, maxPosx, maxposY) != null) {
                        BufferedImage buff = null;
                        try {
                            buff = ImageIO.read(new File("D:\\Java projects\\SovelAndHough\\src\\words\\" + lc + ".png"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        int length = (int) Math.ceil((maxPosx - minX) * 0.1);
/*
                        System.out.println(maxPosx - minX + "   " + length);
*/
/*                        try
                        {
                        createNewWordImageInImageDirectory("last\\" + letterCount + ".png", cropImage(buff, minX-length, 0, maxPosx + length, buff.getHeight()));*/


                        if (minX - length > 0 && maxPosx + length < buff.getWidth() - 1) {
                            createNewWordImageInImageDirectory("last\\" + letterCount + ".png", cropImage(buff, minX - length, 0, maxPosx + length, buff.getHeight()));
                        }/* else if (maxPosx + length > buff.getWidth()-1) {
                            createNewWordImageInImageDirectory("last\\" + letterCount + ".png", cropImage(buff, minX - length, 0, maxPosx, buff.getHeight()));
                        }*/ else {
                            createNewWordImageInImageDirectory("last\\" + letterCount + ".png", cropImage(buff, minX, 0, maxPosx, buff.getHeight()));
                        }
                    }
                }
            }

        }

/*        try {
            matOutput2=ImageIO.read(createNewImageInParentDirectory("MatOut",ImageIO.read(out)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        int letterCount=0;
        for (int posy = 1; posy < matOutput2.getHeight(); posy++) {

            for (int posx = 1; posx < matOutput2.getWidth(); posx++) {
                if(rgbToGrayValue(matOutput2.getRGB(posx,posy))==255)
                {
                    maxPosx=posx;
                    maxposY=posy;
                    minX=posx;
                    minY=posy;
                    getConnectedPixels(posx,posy,255,matOutput);
                    letterCount++;

                    if(cropImage(matInput,minX,minY,maxPosx,maxposY)!=null) {
                        createNewImageInImageDirectory(letterCount + ".png", cropImage(matInput, minX, minY, maxPosx, maxposY));
                    }
                }
            }
        }*/

/*
        createNewImageInParentDirectory("Red.png", matOutput2);
*/
/*
        System.out.println("WordCount :" + letterCount);
*/
    }

    public int getThreshold(int[][] inputImage) {
        int[] colorsHt = new int[256];

        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                colorsHt[inputImage[posx][posy]]++;
            }
        }


/*        colorsHt[0]=8;
        colorsHt[1]=7;
        colorsHt[2]=2;
        colorsHt[3]=6;
        colorsHt[4]=9;
        colorsHt[5]=4;*/

/*
        System.out.println(colorsHt[255] + " && " + colorsHt[0]);
*/

        double totalPix = inputImage.length * inputImage[0].length;
/*
        double[] meanAndVarOfGlo = getMeanAndVariance(Arrays.copyOfRange(colorsHt, 0, colorsHt.length));
*/
        int thresh = 0;
        double maxVari = -9999999;

        for (int k = 0; k < colorsHt.length; k++) {
/*
            System.arraycopy();
*/

            double[] meanOfBothClass = getMeanAndVariance(colorsHt,k);

            if (meanOfBothClass[2] == 0 || meanOfBothClass[3] == 0) {
                continue;
            }
/*
            double[] meanAndVarClassB=getMeanAndVariance(Arrays.copyOfRange(colorsHt,k,colorsHt.length));
*/
/*            double meanOfGlobla = meanAndVarOfGlo[0];
            double probabilitySumOfClassA = meanAndVarClassA[2] / totalPix;
            double vari = (meanOfGlobla * probabilitySumOfClassA) - meanAndVarClassA[0];
            vari = vari * vari;
            vari = vari / probabilitySumOfClassA;
            vari = vari / (1 - probabilitySumOfClassA);*/

            double wa = meanOfBothClass[2] / totalPix;
            double wb = meanOfBothClass[3] / totalPix;
            double meanVari = meanOfBothClass[0] - meanOfBothClass[1];

/*
            System.out.println(meanAndVarClassA[0]+ ":"  + meanAndVarClassB[0] + ":" +  k);
*/

            meanVari = meanVari * meanVari;

/*
            System.out.println(meanVari + ":" + k);
*/
            double vari = meanVari * wa * wb;

/*
          System.out.println(meanOfBothClass[0]+ ":"  + meanOfBothClass[1] + ":" + (meanOfBothClass[0]-meanOfBothClass[1]) + ": " + meanVari+ ": " + vari + ": " + k + ";");
*/

/*
            System.out.println(wa + ":" + wb + ":" + k);
*/
            if (vari > maxVari) {
                maxVari = vari;
                thresh = k;
/*
                System.out.println(vari + ";");
*/
            }

        }

        double[] meanAndVarClassA = getMeanAndVariance(colorsHt,thresh);

/*
        System.out.println(meanAndVarClassA[2] + " &&&&&& " + meanAndVarClassA[3] + ";" + meanAndVarClassA[0] + " &&&& " + meanAndVarClassA[1]);
*/

/*
        System.out.println("Threshold: " + thresh);
*/
        return thresh;
    }

    public double[] getMeanAndVariance(int[] colorArray,int k) {
        double[] meanAndVariance = new double[4];
        int sumClassA = 0, sumOfElementsInA = 0,sumClassB = 0, sumOfElementtsInB=0;
        for (int i = 0; i < k ; i++) {
            sumClassA = sumClassA + (colorArray[i] * i);
            sumOfElementsInA = sumOfElementsInA + colorArray[i];
        }

        for (int i = k; i < colorArray.length ; i++) {
            sumClassB = sumClassB + (colorArray[i] * i);
            sumOfElementtsInB = sumOfElementtsInB + colorArray[i];
        }

        double meanOfA = 0,meanOfB=0;
        if (sumOfElementsInA != 0) {
            meanOfA = (sumClassA * 1.0) / (sumOfElementsInA);
        }

        if (sumOfElementtsInB != 0) {
            meanOfB = (sumClassB * 1.0) / (sumOfElementtsInB);
        }

/*
        System.out.println(sumClassA+"/"+sumOfElementsInA+"="+mean);
*/


/*        for (int i = 0; i <= colorArray.length - 1; i++) {
            double singleVariance = mean - i;
            singleVariance = singleVariance * singleVariance;
            singleVariance = singleVariance * colorArray[i];
            variance = variance + singleVariance;
        }*/

/*        if (sumOfElementsInA != 0) {

            variance = variance / sumOfElementsInA;
        }*/

        meanAndVariance[0] = meanOfA;
        meanAndVariance[1] = meanOfB;
        meanAndVariance[2] = sumOfElementsInA;
        meanAndVariance[3]=sumOfElementtsInB;

        return meanAndVariance;
    }

    private void getConnectedPixels(int posx, int posy, int color, BufferedImage mat) {
        if (posx == 0 || posy == 0 || posx == mat.getWidth() - 1 || posy == mat.getHeight() - 1) {
            return;
        }

/*        if(posx<minX)
        {
            minX=posx;
        }
        if(posy<minY)
        {
            minY=posy;
        }
        if(posy>maxposY)
        {
            maxposY=posy;
        }
        if(posx>maxPosx)
        {
            maxPosx=posx;
        }*/

/*        for(int i=-1;i<2;i++)
        {
            for(int j=-1;j<2;j++)
            {
                if(rgbToGrayValue(mat.getRGB(posx+i,posy+j))==color || (i!=0 && j!=0))
                {
                    mat.setRGB(posx+i,posy+j,new Color(255,0,0).getRGB());
                    getConnectedPixels(posx+i,posy+j,255,mat);
                }
            }
        }*/

        if (rgbToGrayValue(mat.getRGB(posx - 1, posy - 1)) == color) {
            if (posx - 1 < minX) {
                minX = posx - 1;
            }
            if (posy - 1 < minY) {
                minY = posy - 1;
            }
            mat.setRGB(posx - 1, posy - 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx - 1, posy - 1, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx, posy - 1)) == color) {
            if (posy - 1 < minY) {
                minY = posy - 1;
            }
            mat.setRGB(posx, posy - 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx, posy - 1, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx + 1, posy - 1)) == color) {
            if (posx + 1 > maxPosx) {
                maxPosx = posx + 1;
            }
            if (posy - 1 < minY) {
                minY = posy - 1;
            }
            mat.setRGB(posx + 1, posy - 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx + 1, posy - 1, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx - 1, posy)) == color) {
            if (posx - 1 < minX) {
                minX = posx - 1;
            }
            mat.setRGB(posx - 1, posy, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx - 1, posy, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx + 1, posy)) == color) {
            if (posx + 1 > maxPosx) {
                maxPosx = posx + 1;
            }
            mat.setRGB(posx + 1, posy, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx + 1, posy, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx - 1, posy + 1)) == color) {
            if (posy + 1 > maxposY) {
                maxposY = posy + 1;
            }
            if (posx - 1 < minX) {
                minX = posx - 1;
            }

            mat.setRGB(posx - 1, posy + 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx - 1, posy + 1, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx, posy + 1)) == color) {
            if (posy + 1 > maxposY) {
                maxposY = posy + 1;
            }
            mat.setRGB(posx, posy + 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx, posy + 1, 255, mat);
        }
        if (rgbToGrayValue(mat.getRGB(posx + 1, posy + 1)) == color) {
            if (posy + 1 > maxposY) {
                maxposY = posy + 1;
            }
            if (posx + 1 > maxPosx) {
                maxPosx = posx + 1;
            }
            mat.setRGB(posx + 1, posy + 1, new Color(255, 0, 0).getRGB());
            getConnectedPixels(posx + 1, posy + 1, 255, mat);
        }
    }

    public File createNewImageInParentDirectory(String fileName, BufferedImage inputImage) {
        File hough = new File(System.getProperty("user.dir") + "\\src\\" + fileName);
/*
        System.out.println();
*/
        try {
            ImageIO.write(inputImage, "png", hough);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return hough;
    }

    public File createNewImageInImageDirectory(String fileName, BufferedImage inputImage) {
        File hough = new File(System.getProperty("user.dir") + "\\src\\image\\" + fileName);
/*
        System.out.println();
*/
        try {
            ImageIO.write(inputImage, "png", hough);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return hough;
    }

    public File createNewWordImageInImageDirectory(String fileName, BufferedImage inputImage) {
        File hough = new File(System.getProperty("user.dir") + "\\src\\" + fileName);
/*
        System.out.println();
*/
        try {
            ImageIO.write(inputImage, "png", hough);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return hough;
    }

    public int rgbToGrayValue(int rgb) {
        Color pixel = new Color(rgb);
        return pixel.getBlue();
    }

    public int getGrayScaleValue(Color pixel) {
        return (pixel.getBlue() + pixel.getGreen() + pixel.getRed()) / 3;
    }

    private BufferedImage cropImage(BufferedImage src, int startX, int startY, int endX, int endY) {
        if (endX - startX <= 0 || endY - startY <= 0) {
            return null;
        }
        if (startX < 0 || endX > src.getWidth() || endX - startX < 1 || endY - startY < 1) {
            return null;
        }
        BufferedImage dest = src.getSubimage(startX, startY, endX - startX, endY - startY);
        return dest;
    }

}
