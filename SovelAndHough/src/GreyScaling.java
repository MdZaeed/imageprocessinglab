import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GreyScaling {


    /*    int maxR = 0, maxDeg = 0, maxVote = 0;
        String key="";
        HashMap hm = new HashMap();*/
    HashMap coordinates = new HashMap();
    File imageFile;

    int reCount = 0;

    public void rgbToGray() {

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "All Images", "jpg", "gif", "png");
        chooser.setFileFilter(filter);
        chooser.showOpenDialog(null);

        imageFile = chooser.getSelectedFile();

        BufferedImage inputImage = null;
/*        String fileName = "D:/Java projects/SovelAndHough/src/android.jpg";
        String grayImageName = "D:/Java projects/SovelAndHough/src/gray.jpg";
        String houghImageName = "D:/Java projects/SovelAndHough/src/android - Copy.jpg";*/
        try {
//			URL url = getClass().getResource(fileName);
            inputImage = ImageIO.read(imageFile);
/*
            outputImage = ImageIO.read(new File(grayImageName));
*/
/*
            houghOutput= ImageIO.read(new File(houghImageName));
*/
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //Grayscaling
/*        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = getGrayScaleValue(pixel);
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
            }
        }*/

        //Grayscaling
        int[][] grayMatrix = convertToGrayScale(inputImage);
        createImageFromArray("NewGray.png", grayMatrix);


/*        //Binary Tranformation
        int[][] binMatrix = convertToBinary(grayMatrix, 200);
        createImageFromArray("Binary.png", binMatrix);

        //Binary Transformation Using OtsuThresholding
        int thres = getThreshold(grayMatrix);
        int[][] otsuBin = convertToBinary(grayMatrix, thres);
        createImageFromArray("OtsuBin.png", otsuBin);

        //Sobel line detection
        int[][] msobelMaskforX = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
        int[][] msobelMaskforY = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
        int[][] sobelMatrixOfX = convulation(grayMatrix, msobelMaskforX);
        int[][] sobelMatrixOfY = convulation(grayMatrix, msobelMaskforY);
        int[][] sobelMatrixOfXYUnited = doSquareRoot(sobelMatrixOfX, sobelMatrixOfY);
        createImageFromArray("NewSobel.png", sobelMatrixOfXYUnited);*/

        int[][] highBoostMask={{-1,-1,-1},{-1,5,-1},{-1,-1,-1}};

        int[][] secondOrderDerivcative={{0,-1,0},{-1,4,-1},{0,-1,0}};
        int[][] secondOrderImage=newConvulation(grayMatrix,secondOrderDerivcative);
        createImageFromArray("SecondOrderImage.png",secondOrderImage);

        int[][] addedMat=add(grayMatrix,secondOrderImage);
        createImageFromArray("LapAndSharp.png",addedMat);

        int[][] addedHighBoost=newConvulation(grayMatrix,addedMat);
        createImageFromArray("AddedHighBoost.png",addedHighBoost);

        int[][] msobelMaskforX = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};
        int[][] msobelMaskforY = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
        int[][] sobelMatrixOfX = convulation(grayMatrix, msobelMaskforX);
        int[][] sobelMatrixOfY = convulation(grayMatrix, msobelMaskforY);
        int[][] sobelMatrixOfXYUnited = doSquareRoot(sobelMatrixOfX, sobelMatrixOfY);
        createImageFromArray("NewSobel.png", sobelMatrixOfXYUnited);

        int[][] fiveAndFiveAverageFilter={{1,1,1,1,1},{1,1,1,1,1},{1,1,1,1,1},{1,1,1,1,1},{1,1,1,1,1}};
        int[][] averagedImageMatrix=newConvulation(sobelMatrixOfXYUnited,fiveAndFiveAverageFilter);
        createImageFromArray("SobelAveraged.png", averagedImageMatrix);

        int[][] lapSecond={{-1,-1,-1},{-1,8,-1},{-1,-1,-1}};
        int[][] lapSecondMatrix=newConvulation(grayMatrix,lapSecond);
        createImageFromArray("LapSecond.png",lapSecondMatrix);

        int[][] highBoostMaskMatrix=newConvulation(grayMatrix,lapSecond);
        createImageFromArray("HighBoost.png",highBoostMaskMatrix);




/*        //k-means Segementation
        int[] categories = new int[10];
        for (int i = 0; i < 10; i++) {
            categories[i] = (i + 1) * 25;

*//*
            System.out.println(categories[i]);
*//*

        }*/

/*
        categories[0] = 0;
        categories[1] = 100;
        categories[2] = 200;
*/

/*        kMEans(grayMatrix, categories);
        int[] optiumizedCategories2 = optimizeTheCatgories(categories);
        Color[][] segmentedImage = createColorMatrixFromCategories(grayMatrix, optiumizedCategories2);
        createImageFromColorArray("Kmeans.jpg", segmentedImage);
        int[][] segmentedGrayImage= createGrayMatrixFromCategories(grayMatrix,optiumizedCategories2);
        createImageFromArray("Kmeans-Gray.png",segmentedGrayImage);*/

        // Mean Shift Segemntation
/*        int radius=10;
        int[] categoriesForMeanShift = meanShift(grayMatrix,radius);
        int[] optiumizedCategories = optimizeTheCatgories(categoriesForMeanShift);
        Color[][] segmentedByMeanShift = createColorMatrixFromCategories(grayMatrix, optiumizedCategories);
        createImageFromColorArray("MeanShift.jpg", segmentedByMeanShift);
        int[][] segmentedGrayImage2= createGrayMatrixFromCategories(grayMatrix,optiumizedCategories);
        createImageFromArray("Meanshift-Gray.png",segmentedGrayImage2);*/

/*        int[][] rValue = returnRValues(inputImage);
        int[][] bValue = returnBValues(inputImage);
        int[][] gValue = returnGValues(inputImage);*/

/*        int[] rCategories=meanShift(rValue,radius);
        int[] optiumizedCategoriesOfR = optimizeTheCatgories(rCategories);
        int[][] segementByR=createGrayMatrixFromCategories(rValue,optiumizedCategoriesOfR);
    
        int[] gCategories=meanShift(gValue,radius);
        int[] optiumizedCategoriesOfG = optimizeTheCatgories(gCategories);
        int[][] segementByG=createGrayMatrixFromCategories(gValue,optiumizedCategoriesOfG);

        int[] bCategories=meanShift(bValue,5);
        int[] optiumizedCategoriesOfB = optimizeTheCatgories(bCategories);
        int[][] segementByB=createGrayMatrixFromCategories(bValue,optiumizedCategoriesOfB);*/

/*        Color[][] coloredSegment=createImageFromColorArrays(segementByR,segementByG,segementByB);
        createImageFromColorArray("ColorShift.jpg",coloredSegment);*/

//His redeye reduction
/*        Color[][] coloredSkin = skinDetection(rValue, gValue, bValue);
        createImageFromColorArray("SkinDetRGB.jpg", coloredSkin);

        double[][] hues = new double[rValue.length][rValue[0].length];
        double[][] saturation = new double[rValue.length][rValue[0].length];
        double[][] intensities = new double[rValue.length][rValue[0].length];

        for (int posx = 0; posx < rValue.length; posx++) {
            for (int posy = 0; posy < rValue[0].length; posy++) {
                int r = rValue[posx][posy], g = gValue[posx][posy], b = bValue[posx][posy];

                double[] his = RGBtoHIS(r, g, b);
                intensities[posx][posy] = his[0];
                saturation[posx][posy] = his[1];
                hues[posx][posy] = his[2];
            }
        }

        Color[][] colorMat = new Color[rValue.length][rValue[0].length];
        for (int posx = 0; posx < rValue.length; posx++) {
            for (int posy = 0; posy < rValue[0].length; posy++) {
                double h = hues[posx][posy], i = intensities[posx][posy], s = saturation[posx][posy];

                if (h < 45 || h > 315) {
                    if (s > .3) {
                        colorMat[posx][posy] = new Color(0, 0, 0);
                        continue;
                    }
                }
                colorMat[posx][posy] = new Color(rValue[posx][posy], gValue[posx][posy], bValue[posx][posy]);
            }
        }

        createImageFromColorArray("RedDet.jpg", colorMat);

        Color[][] skinMatColor = new Color[rValue.length][rValue[0].length];

        for (int posx = 0; posx < rValue.length; posx++) {
            for (int posy = 0; posy < rValue[0].length; posy++) {
                double h = hues[posx][posy], i = intensities[posx][posy], s = saturation[posx][posy];

                if (h > 0 && h < 50 && s > .23 && s < .68) {
                    skinMatColor[posx][posy] = new Color(0, 0, 0);
                } else {
                    skinMatColor[posx][posy] = new Color(rValue[posx][posy], gValue[posx][posy], bValue[posx][posy]);
                }
            }
        }

        createImageFromColorArray("SkinMatHis.jpg", skinMatColor);*/


        //Harris Corner Detection
/*
        int[][] gaussianMask = {{1, 4, 7, 4, 1}, {4, 16, 26, 16, 4}, {7, 26, 41, 26, 7}, {4, 16, 26, 16, 4}, {1, 4, 7, 4, 1}};
        int[][] sobelMaskOfX2 = multiply(sobelMatrixOfX, sobelMatrixOfX);
        int[][] sobelMaskOfY2 = multiply(sobelMatrixOfY, sobelMatrixOfY);
        int[][] sobelMatrixForXY = multiply(sobelMatrixOfX, sobelMatrixOfY);
        int[][] gauMatOfX2 = convulationWithoutScalingDown(sobelMaskOfX2, gaussianMask);
        int[][] gauMatOfY2 = convulationWithoutScalingDown(sobelMaskOfY2, gaussianMask);
        int[][] gauMatOfXY = convulationWithoutScalingDown(sobelMatrixForXY, gaussianMask);

        int[][] temp1 = multiply(gauMatOfX2, gauMatOfY2);
        int[][] temp2 = multiply(gauMatOfXY, gauMatOfXY);
        temp1 = add(temp1, temp2);
        temp2 = add(gauMatOfX2, gauMatOfY2);
        int[][] cornerMat = divide(temp1, temp2);
        createImageFromArray("Corner.png", cornerMat);

*/

/*        for (int i = 0; i < 10; i++) {
            System.out.println(categories[i]);
        }*/
/*        File out = new File("D:/Java projects/SovelAndHough/src/haha.png");
        try {
            ImageIO.write(outputImage, "png", out);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
/*
        File out=createNewImageInParentDirectory("haha.png",outputImage);

        try {
            inputImage = ImageIO.read(out);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        int[][] xSquare=new int[inputImage.getHeight()][inputImage.getWidth()];
        int[][] ySquare=new int[inputImage.getHeight()][inputImage.getWidth()];
        int[][] xy=new int[inputImage.getHeight()][inputImage.getWidth()];
        for (int posx = 1; posx < inputImage.getWidth() - 1; posx++) {
            for (int posy = 1; posy < inputImage.getHeight() - 1; posy++) {
                int p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                int p2 = rgbToGrayValue(inputImage.getRGB(posx, posy - 1)) * -2;
                int p4 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1)) * -1;
                int p5 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1));
                int p6 = rgbToGrayValue(inputImage.getRGB(posx, posy + 1)) * 2;
                int p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));

                int rawconvu = p1 + p2 + p4 + p5 + p6 + p7, convu;
                convu = rawconvu;


                p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                p2 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy)) * -2;
                p4 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1)) * -1;
                p5 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1));
                p6 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy)) * 2;
                p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));

                int rawconvu2 = p1 + p2 + p4 + p5 + p6 + p7, convu2 = 0;
                convu2 = rawconvu2;


                int convu3 = (int) Math.sqrt((convu * convu + convu2 * convu2));

                if (convu3 > 255) {
                    convu3 = 255;
                } else if (convu3 < 0) {
                    convu3 = 0;
                }

                Color grayPixel = new Color(convu3, convu3, convu3);
                outputImage.setRGB(posx, posy, grayPixel.getRGB());

                xSquare[posx][posy]=convu*convu;
                ySquare[posx][posy]=convu2*convu2;
                xy[posx][posy]=convu*convu2;

*//*                if (convu3 > 100) {
                    for (int i = 1; i <= 180; i=i+10) {
                        int r = (int) (convu * Math.toDegrees(Math.cos(Math.toDegrees(i))) + convu2 * Math.toDegrees(Math.sin(Math.toRadians(i))));
                        int div=(r%100);
                        int rem=100-div;
                        if(div < 100/2)
                        {
                            r=r-rem;
                        }else
                        {
                            r=r+rem;
                        }
                        String key = r + "," + i;

                        try {
                            int count = (int) hm.get(key);
                            count = count + 1;
                            hm.put(key, count);
                            String coordinate=(String)coordinates.get(key);
                            coordinate= coordinate + "," + posx+ "," + posy;
                            coordinates.put(key,coordinate);
                        } catch (Exception e) {
                            hm.put(key, 1);
                            coordinates.put(key,posx+ "," + posy);

*//**//*
                            System.out.println("Line");
*//**//*

                        }
                    }
                }*//*

*//*
                double slope=(int)Math.toDegrees(Math.atan2(rawconvu2,rawconvu));
*//*
*//*
                int intersection= (int)(rawconvu2 - ( slope * rawconvu ));
*//*

*//*
                String key= slope + "," + intersection;
*//*

*//*				if(hm.get("key")==null)
                {
					hm.put(key,1);
				}else
				{
					int count=(int)hm.get(key);
					count=count+1;
					hm.put(key,count);
				}*//*

*//*                int r= (int)(rawconvu * Math.cos(slope) + rawconvu2 * Math.sin(slope));

				String key= slope + "," + r;

				if(hm.get("key")==null)
				{
					hm.put(key,1);
				}else
				{
					int count=(int)hm.get(key);
					count=count+1;
					hm.put(key,count);
				}*//*

*//*
                System.out.println(rawconvu + "," + rawconvu2);
*//*
*//*				Set set = hm.entrySet();
                // Get an iterator
				Iterator i = set.iterator();
				// Display elements
				while(i.hasNext()) {
					Map.Entry me = (Map.Entry)i.next();
					System.out.print(me.getKey() + ": ");
					System.out.println(me.getValue());
				}*//*
            }
        }

        File sovel = new File("D:/Java projects/SovelAndHough/src/sovel.png");
        try {
            ImageIO.write(outputImage, "png", sovel);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

 *//*       for(int xPos=2;xPos<inputImage.getWidth()-2;xPos++)
        {
            for (int yPos=2;yPos<inputImage.getHeight()-2;yPos++)
            {
                int p1 = rgbToGrayValue(inputImage.getRGB(xPos - 2, yPos - 2)) * 1 / 273;
                int p2 = rgbToGrayValue(inputImage.getRGB(xPos - 1, yPos - 2)) * 4 / 273;
                int p3 = rgbToGrayValue(inputImage.getRGB(xPos , yPos - 2)) * 7 / 273;
                int p4 = rgbToGrayValue(inputImage.getRGB(xPos + 1, yPos - 2)) * 4 / 273;
                int p5 = rgbToGrayValue(inputImage.getRGB(xPos + 2, yPos - 2)) * 1 / 273;
                int p6 = rgbToGrayValue(inputImage.getRGB(xPos - 2, yPos - 1)) * 4 / 273;
                int p7 = rgbToGrayValue(inputImage.getRGB(xPos - 1, yPos - 1)) * 16 / 273;
                int p8 = rgbToGrayValue(inputImage.getRGB(xPos , yPos - 1)) * 26 / 273;
                int p9 = rgbToGrayValue(inputImage.getRGB(xPos + 1, yPos - 1)) * 16 / 273;
                int p10 = rgbToGrayValue(inputImage.getRGB(xPos + 2, yPos - 1)) * 4 / 273;
                int p11 = rgbToGrayValue(inputImage.getRGB(xPos - 2, yPos )) * 7 / 273;
                int p12 = rgbToGrayValue(inputImage.getRGB(xPos - 1, yPos )) * 26 / 273;
                int p13 = rgbToGrayValue(inputImage.getRGB(xPos , yPos )) * 41 / 273;
                int p14 = rgbToGrayValue(inputImage.getRGB(xPos + 1, yPos )) * 26 / 273;
                int p15 = rgbToGrayValue(inputImage.getRGB(xPos + 2, yPos )) * 7 / 273;
                int p16 = rgbToGrayValue(inputImage.getRGB(xPos - 2, yPos + 1)) * 4 / 273;
                int p17 = rgbToGrayValue(inputImage.getRGB(xPos - 1, yPos + 1)) * 16 / 273;
                int p18 = rgbToGrayValue(inputImage.getRGB(xPos , yPos + 1)) * 26 / 273;
                int p19 = rgbToGrayValue(inputImage.getRGB(xPos + 1, yPos + 1)) * 16 / 273;
                int p20 = rgbToGrayValue(inputImage.getRGB(xPos + 2, yPos + 1)) * 4 / 273;
                int p21 = rgbToGrayValue(inputImage.getRGB(xPos - 2, yPos + 2)) * 1 / 273;
                int p22 = rgbToGrayValue(inputImage.getRGB(xPos - 1, yPos + 2)) * 4 / 273;
                int p23 = rgbToGrayValue(inputImage.getRGB(xPos , yPos + 2)) * 7 / 273;
                int p24 = rgbToGrayValue(inputImage.getRGB(xPos + 1, yPos + 2)) * 4 / 273;
                int p25 = rgbToGrayValue(inputImage.getRGB(xPos + 2, yPos + 2)) * 1 / 273;

                int gauX=p1+p2+p3+p4+p5+p6+p7+p8+p9+p10+p11+p12+p13+p14+p15+p16+p17+p18+p19+p20+p21+p22+p23+p24+p25;

            }
        }*//*

*//*        Set set = hm.entrySet();
        // Get an iterator
        Iterator i = set.iterator();
        // Display elements
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            if((int)me.getValue()> 1000)
            {
                String rAndDeg=(String)me.getKey();
                String[] rAndD=rAndDeg.split(",");
                maxR= Integer.parseInt(rAndD[0]);
                maxDeg = Integer.parseInt(rAndD[1]);
                maxVote=(int)me.getValue();
                key=rAndDeg;
                drawLines(key,houghOutput);
            }
*//**//*            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());*//**//*
        }

*//**//*
        System.out.println(maxR + ": " + maxDeg + "; " + maxVote);
*//**//*
        File hough = new File("D:/Java projects/SovelAndHough/src/hough.png");
        try {
            ImageIO.write(houghOutput, "png", hough);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    private Color[][] skinDetection(int[][] rValue, int[][] gValue, int[][] bValue) {
        Color[][] colorMat = new Color[rValue.length][rValue[0].length];
        for (int posx = 0; posx < rValue.length; posx++) {
            for (int posy = 0; posy < rValue[0].length; posy++) {
                int r = rValue[posx][posy], g = gValue[posx][posy], b = bValue[posx][posy];
                if (r > 95 && g > 40 && b > 20) {
                    if (Math.max(Math.max(r, g), b) - Math.min(Math.min(r, g), b) > 15) {
                        if (r > g && r > b && r - g > 15) {
                            colorMat[posx][posy] = new Color(r, g, b);
                            continue;
                        }
                    }
                }

                colorMat[posx][posy] = new Color(0, 0, 0);
            }
        }

        return colorMat;
    }

    private Color[][] createImageFromColorArrays(int[][] rValues, int[][] gValues, int[][] bValues) {
        Color[][] returnJinish = new Color[rValues.length][rValues[0].length];

        for (int posx = 0; posx < rValues.length; posx++) {
            for (int posy = 0; posy < rValues[0].length; posy++) {
                Color color = new Color(rValues[posx][posy], gValues[posx][posy], bValues[posx][posy]);
                returnJinish[posx][posy] = color;
            }
        }

        return returnJinish;
    }

    private int[] optimizeTheCatgories(int[] categoriesForMeanShift) {
        int[] newCategory;

        Stack<Integer> temp = new Stack<>();
        for (int i = 0; i < categoriesForMeanShift.length; i++) {
            if (i == 0) {
                temp.push(categoriesForMeanShift[i]);
                continue;
            }

            boolean isDuplicate = false;
            for (int j = 0; j < i; j++) {
                if (categoriesForMeanShift[i] > categoriesForMeanShift[j] - 10 && categoriesForMeanShift[i] < categoriesForMeanShift[j] + 10) {
                    isDuplicate = true;
                    break;
                }
            }

            if (isDuplicate == false) {
                temp.push(categoriesForMeanShift[i]);
            }
        }

        newCategory = new int[temp.size()];
        for (int i = 0; i < newCategory.length && !temp.isEmpty(); i++) {
            newCategory[i] = temp.pop();
        }

        return newCategory;
    }

    private int[][] convertToBinary(int[][] inputImage, int threshHold) {
        int[][] binMat = new int[inputImage.length][inputImage[0].length];
        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                if (inputImage[posx][posy] > threshHold) {
                    binMat[posx][posy] = 255;
                } else {
                    binMat[posx][posy] = 0;
                }

            }
        }
        return binMat;
    }

    private Color[][] createColorMatrixFromCategories(int[][] inputImage, int[] categories) {
        Color[][] outputImageRed = new Color[inputImage.length][inputImage[0].length];
        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                int minDiff = 9999, cat = -1;
                for (int i = 0; i < categories.length; i++) {
                    int diff = categories[i] - inputImage[posx][posy];
                    if (diff < 0) {
                        diff = diff * -1;
                    }
                    if (diff < minDiff) {
                        minDiff = diff;
                        cat = i;
                    }
                }

                Color catColor = returnColor(cat);
                outputImageRed[posx][posy] = catColor;
            }
        }

        return outputImageRed;
    }

    private int[][] createGrayMatrixFromCategories(int[][] inputImage, int[] categories) {
        int[][] outputImageRed = new int[inputImage.length][inputImage[0].length];
        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                int minDiff = 9999, cat = -1;
                for (int i = 0; i < categories.length; i++) {
                    int diff = categories[i] - inputImage[posx][posy];
                    if (diff < 0) {
                        diff = diff * -1;
                    }
                    if (diff < minDiff) {
                        minDiff = diff;
                        cat = i;
                    }

/*                    if(posx==10 && posy==10)
                    {
                        System.out.println(categories[i] + ";" + diff );
                    }*/
                }

/*                if(posx==10 && posy==10)
                {
                    System.out.println(cat + ";" + inputImage[posx][posy]);
                }*/

                outputImageRed[posx][posy] = categories[cat];
            }
        }

        return outputImageRed;
    }

    private Color returnColor(int cat) {
        switch (cat) {
            case 0:
                return new Color(255, 0, 0);

            case 1:
                return new Color(0, 255, 0);

            case 2:
                return new Color(0, 0, 255);
            case 3:
                return new Color(255, 255, 0);
            case 4:
                return new Color(255, 0, 255);
            case 5:
                return new Color(0, 255, 255);
            case 6:
                return new Color(255, 0, 0);
            case 7:
                return new Color(100, 150, 200);
            case 8:
                return new Color(0, 0, 0);
            case 9:
                return new Color(255, 255, 255);

        }
        return null;
    }

    private int[][] doSquareRoot(int[][] xMatrix, int[][] yMatrix) {

        int[][] retMat = new int[xMatrix.length][xMatrix[0].length];

        for (int posx = 0; posx < retMat.length; posx++) {
            for (int posy = 0; posy < retMat[0].length; posy++) {
                int x = (xMatrix[posx][posy] * xMatrix[posx][posy]) + (yMatrix[posx][posy] * yMatrix[posx][posy]);
                x = (int) Math.sqrt(x);

                if (x > 255) {
                    retMat[posx][posy] = 255;
                } else if (x < 0) {
                    retMat[posx][posy] = 0;
                } else {
                    retMat[posx][posy] = x;
                }
            }
        }
        return retMat;
    }

    private int[][] multiply(int[][] xMatrix, int[][] yMatrix) {

        int[][] retMat = new int[xMatrix.length][xMatrix[0].length];

/*
        System.out.println(xMatrix.length + " /" + xMatrix[0].length);
*/


        for (int posx = 0; posx < retMat.length; posx++) {
            for (int posy = 0; posy < retMat[0].length; posy++) {
                double x = xMatrix[posx][posy] * yMatrix[posx][posy];

/*                if (x > 255) {
                    retMat[posx][posy] = 255;
                } else if (x < 0) {
                    retMat[posx][posy] = 0;
                } else {
                    retMat[posx][posy] = x;
                }*/

/*                if(posx==0 && posy==0)
                {
                    System.out.println(x + " t");
                }*/

                retMat[posx][posy] = (int) x;

            }
        }
        return retMat;
    }

    private int[][] add(int[][] xMatrix, int[][] yMatrix) {

        int[][] retMat = new int[xMatrix.length][xMatrix[0].length];

        for (int posx = 0; posx < retMat.length; posx++) {
            for (int posy = 0; posy < retMat[0].length; posy++) {
                int x = xMatrix[posx][posy] + yMatrix[posx][posy];

                if (x > 255) {
                    retMat[posx][posy] = 255;
                } else if (x < 0) {
                    retMat[posx][posy] = 0;
                } else {
                    retMat[posx][posy] = x;
                }

/*
                retMat[posx][posy] = x;
*/

            }
        }
        return retMat;
    }

    private int[][] divide(int[][] xMatrix, int[][] yMatrix) {

        int[][] retMat = new int[xMatrix.length][xMatrix[0].length];

        for (int posx = 0; posx < retMat.length; posx++) {
            for (int posy = 0; posy < retMat[0].length; posy++) {

                int x;
                if (yMatrix[posx][posy] == 0) {
                    x = 0;
                } else {
                    x = xMatrix[posx][posy] / yMatrix[posx][posy];
                }

                if (x > 255) {
                    retMat[posx][posy] = 255;
                } else if (x < 0) {
                    retMat[posx][posy] = 0;
                } else {
                    retMat[posx][posy] = x;
                }
            }
        }
        return retMat;
    }

    public int getGrayScaleValue(Color pixel) {
        return (pixel.getBlue() + pixel.getGreen() + pixel.getRed()) / 3;
    }

    public int rgbToGrayValue(int rgb) {
        Color pixel = new Color(rgb);
        return pixel.getBlue();
    }

    public File createNewImageInParentDirectory(String fileName, BufferedImage inputImage) {
        File hough = new File(System.getProperty("user.dir") + "\\src\\" + fileName);
/*
        System.out.println();
*/
        try {
            ImageIO.write(inputImage, "png", hough);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return hough;
    }

    public void drawLines(String key, BufferedImage houghOutput) {
        String coors = (String) coordinates.get(key);
/*
        System.out.println(coors);
*/
        String[] individualCoors = coors.split(",");

        for (int k = 0; k < individualCoors.length; k = k + 2) {
            int xpos = Integer.parseInt(individualCoors[k]);
            int ypos = Integer.parseInt(individualCoors[k + 1]);
            Color grayPixel = new Color(255, 0, 0);
            houghOutput.setRGB(xpos, ypos, grayPixel.getRGB());
/*
            System.out.println(xpos + "," + ypos);
*/
        }
    }

    public int[][] convertToGrayScale(BufferedImage inputImage) {
        int[][] imagePixels = new int[inputImage.getWidth()][inputImage.getHeight()];
        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = getGrayScaleValue(pixel);
                imagePixels[posx][posy] = grayValue;
/*
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
*/
/*
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
*/

            }
        }

        return imagePixels;
    }

    public int[][] returnRValues(BufferedImage inputImage) {
        int[][] imagePixels = new int[inputImage.getWidth()][inputImage.getHeight()];
        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = pixel.getRed();
                imagePixels[posx][posy] = grayValue;
/*
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
*/
/*
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
*/

            }
        }

        return imagePixels;
    }

    public int[][] returnBValues(BufferedImage inputImage) {
        int[][] imagePixels = new int[inputImage.getWidth()][inputImage.getHeight()];
        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = pixel.getBlue();
                imagePixels[posx][posy] = grayValue;
/*
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
*/
/*
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
*/

            }
        }

        return imagePixels;
    }

    public int[][] returnGValues(BufferedImage inputImage) {
        int[][] imagePixels = new int[inputImage.getWidth()][inputImage.getHeight()];
        for (int posx = 0; posx < inputImage.getWidth(); posx++) {

            for (int posy = 0; posy < inputImage.getHeight(); posy++) {
                Color pixel = new Color(inputImage.getRGB(posx, posy));
                int grayValue = pixel.getGreen();
                imagePixels[posx][posy] = grayValue;
/*
                Color grayPixel = new Color(grayValue, grayValue, grayValue);
*/
/*
                outputImage.setRGB(posx, posy, grayPixel.getRGB());
*/

            }
        }

        return imagePixels;
    }

    public void createImageFromArray(String fileName, int[][] imageMatrics) {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int posx = 0; posx < imageMatrics.length; posx++) {

            for (int posy = 0; posy < imageMatrics[0].length; posy++) {
                Color color = new Color(imageMatrics[posx][posy], imageMatrics[posx][posy], imageMatrics[posx][posy]);
                bufferedImage.setRGB(posx, posy, color.getRGB());

/*
                System.out.println(posx + " ," + posy + ":" + imageMatrics[posx][posy] );
*/
            }
        }

        createNewImageInParentDirectory(fileName, bufferedImage);
    }

    public void createImageFromColorArray(String fileName, Color[][] imageMatrics) {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(imageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int posx = 0; posx < imageMatrics.length; posx++) {

            for (int posy = 0; posy < imageMatrics[0].length; posy++) {
                Color color = imageMatrics[posx][posy];
                bufferedImage.setRGB(posx, posy, color.getRGB());

/*
                System.out.println(posx + " ," + posy + ":" + imageMatrics[posx][posy] );
*/
            }
        }

        createNewImageInParentDirectory(fileName, bufferedImage);
    }

    public int[][] convulation(int[][] inputImage, int[][] mask) {
        int makSum = 0;
        for (int maskX = 0; maskX < mask.length; maskX++) {
            for (int maskY = 0; maskY < mask.length; maskY++) {
                makSum = makSum + mask[maskX][maskY];
            }
        }
        int[][] convuMat1 = new int[inputImage.length][inputImage[0].length];
        cloneFromArray(inputImage, convuMat1);
        for (int posx = mask[0].length / 2; posx < inputImage.length - (mask[0].length / 2); posx++) {
            for (int posy = mask[0].length / 2; posy < inputImage[0].length - (mask[0].length / 2); posy++) {
                int sum = 0;
                for (int maskX = 0; maskX < mask.length; maskX++) {
                    for (int maskY = 0; maskY < mask.length; maskY++) {
                        int p1 = inputImage[posx + maskY - mask[0].length / 2][posy + maskX - mask[0].length / 2] * mask[maskX][maskY];
                        sum = sum + p1;
 /*                       if(posx==1 && posy==2) {

                            System.out.println((posx+maskY-mask[0].length / 2) + "," + (posy+maskX-mask[0].length / 2) + ":" + inputImage[posx+maskY-mask[0].length / 2][posy+maskX-mask[0].length / 2] + " ," + mask[maskX][maskY] );

*//*
                            System.out.println(inputImage[1][3]+ "");
*//*
                        }*/

/*                        int p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                        int p2 = rgbToGrayValue(inputImage.getRGB(posx, posy - 1)) * -2;
                        int p4 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1)) * -1;
                        int p5 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1));
                        int p6 = rgbToGrayValue(inputImage.getRGB(posx, posy + 1)) * 2;
                        int p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));*/
                    }
                }

                if (makSum != 0) {
                    sum = sum / makSum;
                }
                if (sum > 255) {
                    convuMat1[posx][posy] = 255;
                } else if (sum < -255) {
                    convuMat1[posx][posy] = -255;
                } else {
                    convuMat1[posx][posy] = sum;
                }

/*                System.out.println(sum + ": sum");
                if(posx==1 && posy == 2) {
                    System.out.println(sum + ": sum");
                    convuMat1[10][10]=0;

                    return convuMat1;
                }*/
            }
        }

        return convuMat1;
    }

    public int[][] newConvulation(int[][] inputImage, int[][] mask) {
        int makSum = 0;
        for (int maskX = 0; maskX < mask.length; maskX++) {
            for (int maskY = 0; maskY < mask.length; maskY++) {
                makSum = makSum + mask[maskX][maskY];
            }
        }
        int[][] convuMat1 = new int[inputImage.length][inputImage[0].length];
        cloneFromArray(inputImage, convuMat1);
        for (int posx = mask[0].length / 2; posx < inputImage.length - (mask[0].length / 2); posx++) {
            for (int posy = mask[0].length / 2; posy < inputImage[0].length - (mask[0].length / 2); posy++) {
                int sum = 0;
                for (int maskX = 0; maskX < mask.length; maskX++) {
                    for (int maskY = 0; maskY < mask.length; maskY++) {
                        int p1 = inputImage[posx + maskY - mask[0].length / 2][posy + maskX - mask[0].length / 2] * mask[maskX][maskY];
                        sum = sum + p1;
 /*                       if(posx==1 && posy==2) {

                            System.out.println((posx+maskY-mask[0].length / 2) + "," + (posy+maskX-mask[0].length / 2) + ":" + inputImage[posx+maskY-mask[0].length / 2][posy+maskX-mask[0].length / 2] + " ," + mask[maskX][maskY] );

*//*
                            System.out.println(inputImage[1][3]+ "");
*//*
                        }*/

/*                        int p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                        int p2 = rgbToGrayValue(inputImage.getRGB(posx, posy - 1)) * -2;
                        int p4 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1)) * -1;
                        int p5 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1));
                        int p6 = rgbToGrayValue(inputImage.getRGB(posx, posy + 1)) * 2;
                        int p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));*/
                    }
                }

                if (makSum != 0) {
                    sum = sum / makSum;
                }
                if (sum > 255) {
                    convuMat1[posx][posy] = 255;
                } else if (sum < 0) {
                    convuMat1[posx][posy] = 0;
                } else {
                    convuMat1[posx][posy] = sum;
                }

/*                System.out.println(sum + ": sum");
                if(posx==1 && posy == 2) {
                    System.out.println(sum + ": sum");
                    convuMat1[10][10]=0;

                    return convuMat1;
                }*/
            }
        }

        return convuMat1;
    }

    public int[][] convulationWithoutScalingDown(int[][] inputImage, int[][] mask) {
        int makSum = 0;
        for (int maskX = 0; maskX < mask.length; maskX++) {
            for (int maskY = 0; maskY < mask.length; maskY++) {
                makSum = makSum + mask[maskX][maskY];
            }
        }

/*
        System.out.println(makSum+ ";");
*/
        int[][] convuMat1 = new int[inputImage.length][inputImage[0].length];
        cloneFromArray(inputImage, convuMat1);
        for (int posx = mask[0].length / 2; posx < inputImage.length - (mask[0].length / 2); posx++) {
            for (int posy = mask[0].length / 2; posy < inputImage[0].length - (mask[0].length / 2); posy++) {
                int sum = 0;
                for (int maskX = 0; maskX < mask.length; maskX++) {
                    for (int maskY = 0; maskY < mask.length; maskY++) {
                        int p1 = inputImage[posx + maskY - mask[0].length / 2][posy + maskX - mask[0].length / 2] * mask[maskX][maskY];
                        sum = sum + p1;
 /*                       if(posx==1 && posy==2) {

                            System.out.println((posx+maskY-mask[0].length / 2) + "," + (posy+maskX-mask[0].length / 2) + ":" + inputImage[posx+maskY-mask[0].length / 2][posy+maskX-mask[0].length / 2] + " ," + mask[maskX][maskY] );

*//*
                            System.out.println(inputImage[1][3]+ "");
*//*
                        }*/

/*                        int p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                        int p2 = rgbToGrayValue(inputImage.getRGB(posx, posy - 1)) * -2;
                        int p4 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1)) * -1;
                        int p5 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1));
                        int p6 = rgbToGrayValue(inputImage.getRGB(posx, posy + 1)) * 2;
                        int p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));*/
                    }
                }

                if (makSum != 0) {
                    sum = sum / makSum;
                }
/*                if (sum > 255) {
                    convuMat1[posx][posy] = 255;
                } else if (sum < -255) {
                    convuMat1[posx][posy] = -255;
                } else {
                    convuMat1[posx][posy] = sum;
                }*/

                convuMat1[posx][posy] = sum;


/*                System.out.println(sum + ": sum");
                if(posx==1 && posy == 2) {
                    System.out.println(sum + ": sum");
                    convuMat1[10][10]=0;

                    return convuMat1;
                }*/
            }
        }

        return convuMat1;
    }

    public int[][] gauConvulation(int[][] inputImage, int[][] mask) {
        int[][] convuMat1 = new int[inputImage.length][inputImage[0].length];
        cloneFromArray(inputImage, convuMat1);
        for (int posx = mask[0].length / 2; posx < inputImage.length - (mask[0].length / 2); posx++) {
            for (int posy = mask[0].length / 2; posy < inputImage[0].length - (mask[0].length / 2); posy++) {
                int sum = 0;
                for (int maskX = 0; maskX < mask.length; maskX++) {
                    for (int maskY = 0; maskY < mask.length; maskY++) {
                        int p1 = inputImage[posx + maskY - mask[0].length / 2][posy + maskX - mask[0].length / 2] * mask[maskX][maskY];
                        sum = sum + p1;
 /*                       if(posx==1 && posy==2) {

                            System.out.println((posx+maskY-mask[0].length / 2) + "," + (posy+maskX-mask[0].length / 2) + ":" + inputImage[posx+maskY-mask[0].length / 2][posy+maskX-mask[0].length / 2] + " ," + mask[maskX][maskY] );

*//*
                            System.out.println(inputImage[1][3]+ "");
*//*
                        }*/

/*                        int p1 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy - 1)) * -1;
                        int p2 = rgbToGrayValue(inputImage.getRGB(posx, posy - 1)) * -2;
                        int p4 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy - 1)) * -1;
                        int p5 = rgbToGrayValue(inputImage.getRGB(posx - 1, posy + 1));
                        int p6 = rgbToGrayValue(inputImage.getRGB(posx, posy + 1)) * 2;
                        int p7 = rgbToGrayValue(inputImage.getRGB(posx + 1, posy + 1));*/
                    }
                }

/*                if (sum > 255) {
                    convuMat1[posx][posy] = 255;
                } else if (sum < -255) {
                    convuMat1[posx][posy] = -255;
                } else {
                    convuMat1[posx][posy] = sum;
                }*/

/*                System.out.println(sum + ": sum");
                if(posx==1 && posy == 2) {
                    System.out.println(sum + ": sum");
                    convuMat1[10][10]=0;

                    return convuMat1;
                }*/
            }
        }

        return convuMat1;
    }

    private void cloneFromArray(int[][] inputArray, int[][] outputArray) {
        for (int posx = 0; posx < inputArray.length; posx++) {
            for (int posy = 0; posy < inputArray[0].length; posy++) {
                outputArray[posx][posy] = inputArray[posx][posy];
            }
        }
    }


    private void kMEans(int[][] inputImage, int[] categories) {
        reCount++;
        if (reCount == 10) {
            return;
        }
        int[] catSum = new int[categories.length];
        int[] catCount = new int[categories.length];

        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                int minDiff = 9999, cat = -1;
                for (int i = 0; i < categories.length; i++) {
                    int diff = categories[i] - inputImage[posx][posy];
                    if (diff < 0) {
                        diff = diff * -1;
                    }
                    if (diff < minDiff) {
                        minDiff = diff;
                        cat = i;
                    }
                }

                catSum[cat] = catSum[cat] + inputImage[posx][posy];
                catCount[cat] = catCount[cat] + 1;
            }
        }

        for (int i = 0; i < categories.length; i++) {
            if (catCount[i] == 0) {
                continue;
            }
            categories[i] = catSum[i] / catCount[i];
/*
            System.out.println(categories[i] + ";" + catSum[i] + ";" + catCount[i] + ";" + i);
*/
        }

        kMEans(inputImage, categories);
    }

    private int[] meanShift(int[][] inputImage, int radius) {
        int[] colorsHt = getHistogram(inputImage);

        int[] categories = new int[11];

        for (int i = 0; i < 255; i = i + 25) {
            int oldmean = i, newMean;
            while (true) {
                newMean = (int) getMean(colorsHt, oldmean - radius, oldmean + radius);
                if (newMean == oldmean) {
                    break;
                }
                oldmean = newMean;
            }

            categories[(i / 25)] = oldmean;
        }

        return categories;
    }

    private int[] getHistogram(int[][] inputImage) {
        int[] colorsHt = new int[256];

        for (int posx = 0; posx < inputImage.length; posx++) {
            for (int posy = 0; posy < inputImage[0].length; posy++) {
                colorsHt[inputImage[posx][posy]]++;
            }
        }

        return colorsHt;
    }

    public int getThreshold(int[][] inputImage) {
        int[] colorsHt = getHistogram(inputImage);

        double totalPix = inputImage.length * inputImage[0].length;
        int thresh = 0;
        double maxVari = -9999999;

        for (int k = 0; k < colorsHt.length; k++) {
            double[] meanOfBothClass = getMeanAndVariance(colorsHt, k);

            if (meanOfBothClass[2] == 0 || meanOfBothClass[3] == 0) {
                continue;
            }
            double wa = meanOfBothClass[2] / totalPix;
            double wb = meanOfBothClass[3] / totalPix;
            double meanVari = meanOfBothClass[0] - meanOfBothClass[1];

            meanVari = meanVari * meanVari;

            double vari = meanVari * wa * wb;

            if (vari > maxVari) {
                maxVari = vari;
                thresh = k;
            }

        }

        return thresh;
    }

    public double[] getMeanAndVariance(int[] colorArray, int k) {
        double[] meanAndVariance = new double[4];
        int sumClassA = 0, sumOfElementsInA = 0, sumClassB = 0, sumOfElementtsInB = 0;
        for (int i = 0; i < k; i++) {
            sumClassA = sumClassA + (colorArray[i] * i);
            sumOfElementsInA = sumOfElementsInA + colorArray[i];
        }

        for (int i = k; i < colorArray.length; i++) {
            sumClassB = sumClassB + (colorArray[i] * i);
            sumOfElementtsInB = sumOfElementtsInB + colorArray[i];
        }

        double meanOfA = 0, meanOfB = 0;
        if (sumOfElementsInA != 0) {
            meanOfA = (sumClassA * 1.0) / (sumOfElementsInA);
        }

        if (sumOfElementtsInB != 0) {
            meanOfB = (sumClassB * 1.0) / (sumOfElementtsInB);
        }

/*
        System.out.println(sumClassA+"/"+sumOfElementsInA+"="+mean);
*/


/*        for (int i = 0; i <= colorArray.length - 1; i++) {
            double singleVariance = mean - i;
            singleVariance = singleVariance * singleVariance;
            singleVariance = singleVariance * colorArray[i];
            variance = variance + singleVariance;
        }*/

/*        if (sumOfElementsInA != 0) {

            variance = variance / sumOfElementsInA;
        }*/

        meanAndVariance[0] = meanOfA;
        meanAndVariance[1] = meanOfB;
        meanAndVariance[2] = sumOfElementsInA;
        meanAndVariance[3] = sumOfElementtsInB;

        return meanAndVariance;
    }

    public double getMean(int[] colorArray, int from, int to) {
        int sumClassA = 0, sumOfElementsInA = 0;
        for (int i = from; i < to; i++) {
            try {
                sumClassA = sumClassA + (colorArray[i] * i);
                sumOfElementsInA = sumOfElementsInA + colorArray[i];
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }

        double meanOfA = 0;
        if (sumOfElementsInA != 0) {
            meanOfA = (sumClassA * 1.0) / (sumOfElementsInA);
        }

        return meanOfA;
    }

    public double[] RGBtoHIS(int red, int green, int blue) {
        double intensity = (red + green + blue) / 3;

        double min = Math.min(Math.min(red, green), blue);
        double saturation = 0;

        if (intensity != 0) {
            saturation = 1 - (min / intensity);
        }


        double hue = Math.toDegrees(Math.acos((red - .5 * green - .5 * blue) / Math.sqrt((red * red) + (green * green) + (blue * blue) - (red * green) - (red * blue) - (green * blue))));
        if (green < blue) {
            hue = 360 - hue;
        }

        double[] his = new double[3];
        his[0] = intensity;
        his[1] = saturation;
        his[2] = hue;

        return his;

    }
}
